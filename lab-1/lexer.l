%option noyywrap 

%{
    #include <iostream>

    #define DEBUG(x)  std::cerr << __FILE__ << " (" << __LINE__ << ") " << #x << " == " << x << std::endl;    
%}


%%
^(19(0[1-9]|[1-9][0-9]))|2000$ {
    // regex for 20th century year
    std::cout << "20th century, year: " << yytext << std::endl;
}

^([0-9]{11})$ {
    // regex for PESEL
    std::cout << "PESEL: " << yytext << std::endl;
}

^([\-\+]?[0-9]+\.[0-9]*(e[\+\-][0-9]*)?)$ {
    // regex for float
    std::cout << "float: " << yytext << std::endl;
}

bye {  return 0; }

. { /**/ }

%%

int main() 
{
    yyin = fopen("input.txt", "r");
    yylex();
    return 0;
}


