%option noyywrap 

%{
    #include <iostream>
    #include <vector>

    #define DEBUG(x)  std::cerr << __FILE__ << " (" << __LINE__ << ") " << #x << " == " << x << std::endl;    

    char dir[] = "./tex/";
    std::string current_file;

    int g_comment_length_warn = 132;
    unsigned long long g_comment_char_count = 0;
    unsigned long long g_word_count = 0;
    unsigned long long g_sentence_count = 0;
    unsigned long long g_line_count = 0;
    unsigned long long g_files_count = 0;
    unsigned long long g_paragraph_count = 0;
    bool g_warning_displayed = false;
    bool g_seek_par = true;

    std::vector<std::string> files_backup;
    std::vector<int> line_backup;
%}

%x COMMENT
%x CMD_SECTION
%x INCLUDE

%%

(^\include) { std::cout << "regex include" << std::endl; }

"\include{" {
    BEGIN(INCLUDE); 
}

<INCLUDE>[\s\t]* { /* eat the whitespace */ }
<INCLUDE>[^\}\s\t\n]+ { /* got the include file name */
    char* filename = (char*)malloc(strlen(yytext)+strlen(dir) + 2);
    strcpy(filename, dir);
    strcat(filename, yytext);

    yyin = fopen(filename, "r");
    if (!yyin) {
        std::cout << "ERROR! Could not open file!" << std::endl;
        exit(1);
    }
    files_backup.push_back(std::string(filename));
    line_backup.push_back(1);

    yypush_buffer_state(yy_create_buffer(yyin, YY_BUF_SIZE));
    BEGIN(INITIAL);

    free(filename);
}

<<EOF>> {
    g_line_count++;
    g_files_count++;
    line_backup.pop_back();
    files_backup.pop_back();
    yypop_buffer_state();
    if (!YY_CURRENT_BUFFER) {
        yyterminate();
    }
}

"%" { BEGIN(COMMENT); }
<COMMENT>. { 
    g_comment_char_count++;
    if (g_comment_char_count >= g_comment_length_warn && !g_warning_displayed) {
        std::cout << files_backup[files_backup.size()-1] << ":" << line_backup[line_backup.size()-1] << 
            " WARNING! Comment longer than " << g_comment_length_warn << " chars!" << std::endl;
        g_warning_displayed = true;
    }
}
<COMMENT>\n {
    g_line_count++;
    line_backup[line_backup.size()-1]++;
    g_comment_char_count = 0;
    g_warning_displayed = false;
    BEGIN(INITIAL);
}

"\begin" { BEGIN(CMD_SECTION); }
<CMD_SECTION>.* { /* skip all characters */ }
<CMD_SECTION>\n {
    line_backup[line_backup.size()-1]++;
    g_line_count++;
}
"\end" { BEGIN(INITIAL); }

([A-Z]*[a-z]*) {
    if (g_seek_par) {
        g_seek_par = false;
        g_paragraph_count++;
    }
    g_word_count++;
}

(\.) {
    g_sentence_count++;
}

^((\r\n)|\r|\n)$ {
    g_seek_par = true;
}

(((\r\n)|\r|\n)) {
    line_backup[line_backup.size()-1]++;
    g_line_count++;
}

. { /* skip any sign */ }

%%

int main() 
{
    char* filename = (char*)malloc(strlen("p.tex")+strlen(dir) + 2);
    strcpy(filename, dir);
    strcat(filename, "p.tex");
    files_backup.push_back(std::string(filename));
    line_backup.push_back(1);

    yyin = fopen(filename, "r");
    yylex();
    std::cout << "words: " << g_word_count << 
        "\nsentences: " << g_sentence_count <<
        "\nlines: " << g_line_count <<
        "\nparagraphs: " << g_paragraph_count <<
        "\nfiles: " << g_files_count <<
        std::endl;

    free(filename);
    return 0;
}


