%option noyywrap

%{
    #include <stdlib.h> 
    #include <iostream>
    #include "parser.tab.h"

    int line = 1;
%}

%%

"exit"          { 
    std::cout << "exit" << std::endl;
    return EXIT;
}
(OK)    { std::cout << "line " << line << ": " << "OK" << std::endl;}
(ERR)   { std::cout << "line " << line << ": " << "ERROR" << std::endl;}
[\n]    { line++;}
.       {}

%%
