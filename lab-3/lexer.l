



%option noyywrap

%{
   #include <stdlib.h> 
   #include <iostream>
	#include "parser.tab.h"

   int line = 0;
   bool first_line = true;
%}

%%

bye            { return BYE; }

[a-c]          { return C_VALUE; }

[0-9]+         {
                  yylval.iVal = atoi(yytext);
                  return I_VALUE;
               }

"SUM" { return SUM; }

([0-9]+\.[0-9]+)  {
                     yylval.fVal = atof(yytext);
                     return D_VALUE;
                  }

[\n|\r|\n\r]   { 
                  return ENDL;
               }

"+"   { return yytext[0]; }
"-"   { return yytext[0]; }
"*"   { return yytext[0]; }
"/"   { return yytext[0]; }
"^"   { return yytext[0]; }
"("   { return yytext[0]; }
")"   { return yytext[0]; }
"="   { return yytext[0]; }

.  { /* std::cout << yytext << std::endl; */ }

%%
