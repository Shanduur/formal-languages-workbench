%{
    #include <stdlib.h>
    #include <stdio.h>
    #include <iostream>
    #include <exception>
    #include <cmath>
    
    void yyerror(char *s);
    int yylex();
    extern FILE* yyin;

    float   suma;
    int     linia = 0;
    float   var['z' - 'a'] = {0};
    bool    var_exists['z' - 'a'] = {false};
%}

%union {
    float fVal;
    int iVal;
    char cVal;
}

%verbose 

%token BYE ENDL SUM
%token <fVal> D_VALUE
%token <iVal> I_VALUE
%token <cVal> C_VALUE
%type <fVal> sum_value_list sum_expr expr

%left "+" "-"
%left "*" "/"
%right "^"
%left NEG

%%

head    : head BYE  {  
                       std::cerr << "[BYE!]" << std::endl;
                       return 0;
                    }
        | ENDL  { std::cerr << "[endl]" << std::endl; }
        | head ENDL { std::cerr << "[endl]" << std::endl; }
        | head expr ENDL { std::cout << "[expr] " << $2  << std::endl; }
        | head sum_expr ENDL { std::cout << "[suma] " << suma << std::endl; }
        | error ENDL

expr    : I_VALUE   { $$ = $1; }
        | D_VALUE   { $$ = $1; }
        | C_VALUE   { $$ = $1; }
        | C_VALUE '=' expr  {
                                // if (var_exists[$1]) {
                                    // std::cout << "[WARN] overwrite! " << std::endl;
                                // }
                                var[$1] = $3;
                                var_exists[$1] = $3;
                            }
        | '-' expr %prec NEG { $$ = -$2; }
        | expr '+' expr { $$ = $1 + $3; }
        | expr '-' expr { $$ = $1 - $3; }
        | expr '*' expr { $$ = $1 * $3; }
        | expr '/' expr { 
                            if ($3 == 0) {
                                // std::cerr << "[WARN] div by zero! " << std::endl;
                                $$ = 0;
                            }
                            else {
                                $$ = $1 / $3;
                            }
                        }
        | expr '^' expr { $$ = std::pow($1, $3); }
        | '(' expr ')'  { $$ = $2; }
        | sum_expr

sum_expr : SUM '(' sum_value_list ')'   { 
                                            $$ = suma; 
                                            suma = 0;
                                        }

sum_value_list  : expr { suma += $1; }
                | sum_value_list ',' expr { suma += $3; }

%%

void yyerror(char *s) {
    std::cerr << "[ERRO] " << s << std::endl;
}

int main(int argc, char **argv) 
{
    std::ios_base::sync_with_stdio(true);
    if (argc < 2) {
        std::cerr << "what the fuck bro" << std::endl;
        return 1;
    }
    yyin = fopen(argv[1], "r");
    yyparse();
    return 0;
}
